import React, {Component} from 'react'
import PropTypes from 'prop-types'

class PhoneInput extends Component {
    constructor (props) {
        super(props);
        this.state = {
            phone: this.props.value || "",
            error: false,
            showError: false
        }
    }
    componentWillMount() {
        this.validatePhoneNumber(true);
    }
    componentDidMount() {
        if(this.state.error){
            this.props.registerRequiredInputs("phoneValid", false);
        }else{
            this.props.registerRequiredInputs("phoneValid", true);
        }
    }
    
    static propTypes = {
        label: PropTypes.string
    }
    
    validatePhoneNumber = (isRegistration) => {
        if(typeof isRegistration == "object"){
            this.setState({showError: true});
        }
        const value = this.state.phone;
        if(value.length > 0){
            this.props.handlErrors("phoneValid", true);
        }else{
            this.setState({error: "Укажите номер телефона"});
            this.props.handlErrors("phoneValid", false);
        }
    }
    
    changePhoneNumberState = (e) => {
        const regexp = /[^0-9]/;
        const value = e.target.value;
        
        const result = value.replace(regexp, "");
        this.setState({error: false, phone: result});
    }
    
    toggleErrors = (e) => {
        if(this.state.showError && this.state.error){
            return <div className="form__error">{this.state.error}</div>
        }
    }
    
    render(){
        const {label} = this.props;
        
        return(
            <div className={'form__group form__group-' + (this.state.error && this.state.showError ? 'has_error':'valid') + ' form__group-' + (this.state.phone.length > 0 ? 'full' : 'empty')}>
                <div className="form__label">{label}</div>
                <input className="form__field" type="text"  placeholder="Укажите номер телефона" value={this.state.phone} onBlur={this.validatePhoneNumber} onChange={this.changePhoneNumberState}/>
                {this.toggleErrors()}
            </div>
        )
    }
}

export default PhoneInput