import React, {Component} from 'react'
import PropTypes from 'prop-types'

class TextInput extends Component {
    static propTypes = {
        label: PropTypes.string,
        required: PropTypes.bool,
        disabled: PropTypes.bool,
        value: PropTypes.string,
        inputName: PropTypes.string,
        tagName: PropTypes.string,
        placeholder: PropTypes.string,
    }
    
    constructor (props) {
        super(props);
        this.state = {
            value: this.props.value || "",
            error: false,
            showError: false
        }
    }

    componentWillMount() {
        this.validateInput(true);
    }
    componentDidMount() {
        if(this.state.error){
            this.props.registerRequiredInputs(this.props.inputName, false);
        }else{
            this.props.registerRequiredInputs(this.props.inputName, true);
        }
    }
    
    validateInput = (isRegistration) => {
        if(typeof isRegistration == "object"){
            this.setState({showError: true});
        }
        if(this.props.required){
          const value = this.state.value;
            if(value.length > 0){
                this.props.handlErrors(this.props.inputName, true);
            }else{
                this.setState({error: "Поле "+this.props.label+" обязательно для заполнения"}); 
                this.props.handlErrors(this.props.inputName, false);
            }
        }
        
    }
    
    changeInput = (e) => {
        const value = e.target.value;
        this.setState({error: false, value: value});
    }
    
    toggleErrors = (e) => {
        if(this.state.showError && this.state.error){
            return <div className="form__error">{this.state.error}</div>
        }
    }
    
    insertInput = (e) => {
        if(this.props.tagName == "input"){
            return <input className="form__field" name={this.props.inputName} type="text"  placeholder={this.props.placeholder} value={this.state.value} onBlur={this.validateInput} onChange={this.changeInput} disabled={this.props.disabled}/>
        }else{
            return <textarea className="form__field" name={this.props.inputName} type="text"  placeholder={this.props.placeholder} value={this.state.value} onBlur={this.validateInput} onChange={this.changeInput} disabled={this.props.disabled} rows="4" maxLength="200"></textarea>
        }
    }
    
    render(){
        const {label} = this.props;
        return(
            <div className={'form__group form__group-' + (this.state.error && this.state.showError ? 'has_error':'valid') + ' form__group-' + (this.state.value.length > 0 ? 'full' : 'empty')}>
                <div className="form__label">{label}</div>
                {this.insertInput()}
                {this.toggleErrors()}
            </div>
        )
    }
}

export default TextInput