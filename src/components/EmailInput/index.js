import React, {Component} from 'react'
import PropTypes from 'prop-types'

class EmailInput extends Component {
    constructor (props) {
        super(props);
        this.state = {
            email: "",
            error: false,
            showError: false
        }
    }
    
    componentWillMount() {
        this.validateEmail(true);
    }
    componentDidMount() {
        if(this.state.error){
            this.props.registerRequiredInputs("emailValid", false);
        }else{
            this.props.registerRequiredInputs("emailValid", true);
        }
    }
    
    static propTypes = {
        label: PropTypes.string
    }
    
    
    validateEmail = (isRegistration) => {
        if(typeof isRegistration == "object"){
            this.setState({showError: true});
        }
        const value = this.state.email;
        const isFilled = value.length > 0 ? true : false;
        if(isFilled){
            const regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            const isEmailValid = regexp.test(value);
            if(!isEmailValid){
                this.setState({error: "Укажите корректный email"}); 
                this.props.handlErrors("emailValid", false);
            }else{
                this.props.handlErrors("emailValid", true);
            }
        }else{
            this.setState({error: "Поле обязательно для заполнения"});
                this.props.handlErrors("emailValid", false);
        }
    }
    
    changeEmailState = (e) => {
        const value = e.target.value;
        this.setState({error: false, email: value});
    }
    
    toggleErrors = (e) => {
        if(this.state.showError && this.state.error){
            return <div className="form__error">{this.state.error}</div>
        }
    }
    
    render(){
        const {label} = this.props;
        
        return(
            <div className={'form__group form__group-' + (this.state.error && this.state.showError ? 'has_error':'valid') + ' form__group-' + (this.state.email.length > 0 ? 'full' : 'empty')}>
                <div className="form__label">{label}</div>
                <input className="form__field" type="text"  placeholder="Укажите email" value={this.state.email} onBlur={this.validateEmail} onChange={this.changeEmailState}/>
                {this.toggleErrors()}
            </div>
        )
    }
}

export default EmailInput