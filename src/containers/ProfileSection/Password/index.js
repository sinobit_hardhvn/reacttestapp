import React, {Component} from 'react'
import PropTypes from 'prop-types'

class Password extends Component {

    constructor (props) {
        super(props);
        this.state = {
            newPass: "",
            oldPass: "",
            repeatPass: "",
            error: false
        }
    }

    static propTypes = {

    }


    validateOldPass = (e) => {
        if(e.target.value.length > 0){
            this.setState({error: false})
        }else{
            this.setState({error: "Укажите старый пароль"})
        }
    }
    validateNewPass = (e) => {
        if(e.target.value.length > 0){
            this.setState({error: false})
        }else{
            this.setState({error: "Укажите новый пароль"})
        }
    }

    validateRepeatPass = (e) => {
        if(this.state.repeatPass == this.state.newPass){
            this.setState({error: false})
        }else{
            this.setState({error: "Не верный пароль"})

        }
    }
    changeoldPass = (e) => {
        this.setState({oldPass: e.target.value});
    }
    changeNewPass = (e) => {
        this.setState({newPass: e.target.value});
    }
    changeRepeatPass = (e) => {
        this.setState({repeatPass: e.target.value});
    }

    toggleErrors = (e) => {
        if(this.state.error){
            return <div className="form__error">{this.state.error}</div>
        }
    }

    validateForm = (e) => {
        if(this.state.error){
            e.preventDefault();
            console.log("Форма содержит ошибки")
        }
    }

    render(){

        return(
            <div>
                <form onSubmit={this.validateForm}>
                    <div className="form__block form__block-horizontal">
                        <div className='form__group'>
                            <input className="form__field" placeholder="Укажите старый пароль" type="password" value={this.state.oldPass} onChange={this.changeoldPass} onBlur={this.validateOldPass} />
                        </div>
                        <div className='form__group'>
                            <input className="form__field" placeholder="Укажите новый пароль" type="password" value={this.state.newPass} onChange={this.changeNewPass} onBlur={this.validateNewPass}/>
                        </div>
                        <div className='form__group'>
                            <input className="form__field" placeholder="Повторите новый пароль" type="password" value={this.state.repeatPass} onChange={this.changeRepeatPass} onBlur={this.validateRepeatPass}/>
                        </div>
                        {this.toggleErrors()}
                    </div>
                    <div className="form__footer">
                        <button className="form__btn form__btn-green" type="submit">Сохранить</button>
                    </div>
                </form>
            </div>
        )
    }

}

export default Password