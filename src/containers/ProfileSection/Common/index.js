import React, {Component} from 'react'
import PropTypes from 'prop-types'
import update from 'immutability-helper';
import EmailInput from '../../../components/EmailInput'
import PhoneInput from '../../../components/PhoneInput'
import TextInput from '../../../components/TextInput'
import './style.scss'


class Common extends Component {
    constructor (props) {
        super(props);
        this.state = {
            requiredInputs: {}
        }
        this.handlErrors = this.handlErrors.bind(this)
        this.registerRequiredInputs = this.registerRequiredInputs.bind(this)
    }
    
    registerRequiredInputs = (type, status) => {
        let requiredInputs = this.state.requiredInputs;
        requiredInputs[type] = status;
        this.setState({requiredInputs, requiredInputs});
    }
    
    handlErrors = (type, status) => {
        let error = {};
        error[type] = {$set: status};
        const requiredInputs = update(this.state.requiredInputs, error);
        this.setState({requiredInputs: requiredInputs});
    }
    
    validateForm = (e) => {
        e.preventDefault();
        let validateFields = [];
        let formIsValid = function(element, index, array) {
            return element
        }
        for (let value in this.state.requiredInputs){
            validateFields.push(this.state.requiredInputs[value]);
        }
        console.log(validateFields.every(formIsValid))
    }

    static propTypes = {
        
    }

    render(){

        return(
            <div className="form__wrapper">
               <form action="/" onSubmit={this.validateForm}>
                  <div className="form__block">
                      <TextInput registerRequiredInputs={this.registerRequiredInputs} label="Наименование организации" required={true} disabled={true} value="demo" inputName="company" tagName="input" placeholder="Укажите наименование организации" handlErrors={this.handlErrors}/>
                      <TextInput registerRequiredInputs={this.registerRequiredInputs} label="Адрес" required={true} disabled={false} inputName="adress" tagName="textarea" placeholder="Укажите адрес" handlErrors={this.handlErrors}/>
                  </div>
                   <div className="form__block form__block-horizontal">
                      <EmailInput registerRequiredInputs={this.registerRequiredInputs} handlErrors={this.handlErrors} label="E-mail"  />
                      <PhoneInput registerRequiredInputs={this.registerRequiredInputs} handlErrors={this.handlErrors} label="Телефон" />
                      <TextInput registerRequiredInputs={this.registerRequiredInputs} label="ИНН" required={true} disabled={true} value="1234567891" inputName="inn" tagName="input" placeholder="Укажите ИНН" handlErrors={this.handlErrors}/> 
                   </div>
                   <div className="form__footer">
                       <button className="form__btn form__btn-green" type="submit">Сохранить</button>
                   </div>
                   
               </form>
            </div>
        )
    }

}

export default Common